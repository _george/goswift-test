package ee.zalizko.george.goswift;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-config.xml" })
public class ApplicationContextTest {

	@Test
	public void contextTest() throws Exception {
	}

}
