package ee.zalizko.george.goswift.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Georgii Zalizko
 * 
 */
@Getter
@Setter
@ToString
public class SentEmail {

	Date date;

	String email;

	String text;
}
