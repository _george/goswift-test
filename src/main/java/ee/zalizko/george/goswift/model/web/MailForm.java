package ee.zalizko.george.goswift.model.web;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Georgii Zalizko
 * 
 */

@Getter
@Setter
public class MailForm {

	@NotEmpty
	String email;

	@NotEmpty
	String text;
}
