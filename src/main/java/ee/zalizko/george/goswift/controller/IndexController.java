package ee.zalizko.george.goswift.controller;

import java.util.Date;

import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ee.zalizko.george.goswift.dao.SentEmailDao;
import ee.zalizko.george.goswift.model.SentEmail;
import ee.zalizko.george.goswift.model.web.MailForm;

/**
 * @author Georgii Zalizko
 * 
 */
@Controller
@Slf4j
public class IndexController {

	@Autowired
	SentEmailDao sentEmailDao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		log.debug("index page");
		model.addAttribute(new MailForm());
		return "homepage";

	}

	private void saveMessage(MailForm form) {
		SentEmail sentEmail = new SentEmail();

		sentEmail.setDate(new Date());
		sentEmail.setEmail(form.getEmail());
		sentEmail.setText(form.getText());

		sentEmailDao.add(sentEmail);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String sendMail(@Valid MailForm form, BindingResult result, Model model) {
		log.debug("prepare for saving");

		if (result.hasErrors()) {
			log.debug("has errors");
		} else {
			log.debug("no errors");
			saveMessage(form);
			model.addAttribute("sent", true);
		}

		return "homepage";
	}

}
