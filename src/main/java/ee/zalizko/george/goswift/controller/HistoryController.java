package ee.zalizko.george.goswift.controller;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ee.zalizko.george.goswift.dao.SentEmailDao;
import ee.zalizko.george.goswift.model.SentEmail;

/**
 * @author Georgii Zalizko
 * 
 */
@Controller
@RequestMapping("/history")
@Slf4j
public class HistoryController {

	@Autowired
	SentEmailDao sentEmailDao;

	@RequestMapping(value = "/{email:.+}", method = RequestMethod.GET)
	public String emailHistory(@PathVariable String email, Model model) {
		log.debug("get sent email history for: {}", email);

		List<SentEmail> history = sentEmailDao.getHistory(email);

		log.debug("found {} entries", history.size());

		model.addAttribute("sentEmails", history);
		model.addAttribute("email", email);

		return "history/email_history";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		log.debug("getting emails");

		model.addAttribute("emails", sentEmailDao.getDistinctEmailsList());
		return "history/index";
	}
}
