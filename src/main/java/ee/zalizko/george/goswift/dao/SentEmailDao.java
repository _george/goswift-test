package ee.zalizko.george.goswift.dao;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ee.zalizko.george.goswift.dao.mapper.SentEmailRowMapper;
import ee.zalizko.george.goswift.model.SentEmail;

/**
 * @author Georgii Zalizko
 * 
 */
@Repository
@Slf4j
public class SentEmailDao extends JdbcDaoSupport {

	@Autowired
	DataSource dataSource;

	public void add(SentEmail sentEmail) {
		log.debug("add new email into database: {}", sentEmail);

		Object[] args = new Object[] { new Date(), sentEmail.getEmail(), sentEmail.getText() };
		int[] types = new int[] { Types.TIMESTAMP, Types.VARCHAR, Types.VARCHAR };

		int i = this.getJdbcTemplate().update("INSERT INTO sent_emails(date, email, text) VALUES(?, ?, ?)", args, types);

		log.debug("affected {} rows", i);
	}

	public List<String> getDistinctEmailsList() {
		return this.getJdbcTemplate().queryForList("SELECT DISTINCT email FROM sent_emails ORDER BY email ASC", String.class);
	}

	public List<SentEmail> getHistory(String email) {
		Object[] args = new Object[] { email };
		int[] types = new int[] { Types.VARCHAR };
		return this.getJdbcTemplate().query("SELECT * FROM sent_emails WHERE email = ?", args, types, new SentEmailRowMapper());
	}

	@PostConstruct
	public void init() {
		setDataSource(dataSource);
	}
}
