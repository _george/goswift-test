package ee.zalizko.george.goswift.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ee.zalizko.george.goswift.model.SentEmail;

/**
 * @author Georgii Zalizko
 * 
 */
public final class SentEmailRowMapper implements RowMapper<SentEmail> {

	@Override
	public SentEmail mapRow(ResultSet rs, int rowNum) throws SQLException {
		SentEmail email = new SentEmail();
		email.setDate(rs.getTimestamp("date"));
		email.setEmail(rs.getString("email"));
		email.setText(rs.getString("text"));
		return email;
	}
}
