<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="header">
	<div id="logo">
		<spring:message code="logo.text"></spring:message>
	</div>

	<ul>
		<li><a href="<c:url value="/" />">home</a></li>
		<li><a href="<c:url value="/history" />">history</a></li>
	</ul>
</div>



