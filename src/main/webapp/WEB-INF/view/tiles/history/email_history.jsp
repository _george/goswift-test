<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="email-history">
	<caption>
		<spring:message code="history.caption" arguments="${email}" />
	</caption>
	<thead>
		<tr>
			<th><spring:message code="history.date" /></th>
			<th><spring:message code="history.text" /></th>
	</thead>
	<tbody>
		<c:forEach items="${sentEmails}" var="mail">
			<tr>
				<td class="date"><fmt:formatDate pattern="dd.MM.yyyy HH:mm:ss" value="${mail.date}" /></td>
				<td><c:out value="${mail.text}" /></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

