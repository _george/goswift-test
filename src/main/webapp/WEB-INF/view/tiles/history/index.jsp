<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
	<c:when test="${not empty emails }">
		<ul class="emails">
			<c:forEach items="${emails}" var="mail">
				<li><a href="<c:url value="history/${mail}" />"><c:out value="${mail}" /></a></li>
			</c:forEach>
		</ul>
	</c:when>
	<c:otherwise>
		<spring:message code="history.empty" />
	</c:otherwise>
</c:choose>

