<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
	<c:when test="${sent}">
		<h1>
			<spring:message code="email.sent" />
		</h1>
		<a href="<c:url value="/" />">one more</a>
	</c:when>
	<c:otherwise>
		<form:form commandName="mailForm" method="POST">

			<p class="form-field">
				<label for="email">Address:</label>
				<form:input path="email" type="text" name="email" id="email" />
				<form:errors path="email" cssClass="error" element="span" />
			</p>
			<p class="form-field">
				<label for="text">Text:</label>
				<form:textarea path="text" rows="10" cols="50" id="text" name="text"></form:textarea>
				<form:errors path="text" cssClass="error" element="span" />
			</p>
			<p>
				<input type="submit" value="Send"> <input type="reset" value="Reset">
			</p>
		</form:form>
	</c:otherwise>
</c:choose>
