Töö ise oleks järgmine:
Ülesandeks on kirjutada veebirakendus, millega saaks e-mail'i saata. 
Reaalset e-maili pole vaja välja saata ja piisab, kui seda logisse 
lihtalt kirjutada.

Veebiliidese põhivaates on tegemist vormiga, millel on kaks välja 
-'aadressiväli' ja 'sisuväli', ning nupp 'saada'. Pärast saatmist 
kuvatakse teade - "E-mail saadetud".

Põhivaate ülaservas on abi-lingid, milles on vähemalt üks link "ajaloo" 
vaatamiseks. Ajaloo vaade kuvab kõik e-posti aadressid, millele on mail
saadetud. E-posti aadressile klõpsates kuvatakse vaade, kus on kuvatud 
kõik sellele aadressile saadetud mailid.

Kasutama peab vähemalt järgmisi abi-teeke: Hibernate, Spring, Struts2. 
Lähtekoodi halduseks peab kasutama GIT-i, mille repositoorium tuleb 
esitada ülesande lahendusena. Tarkvaraprojekti haldus-skriptina peab
kasutama Maven-it.

Pööra just tähelepanu sellele viimasele osale ehk neile 
arendusvahenditele ja nende kasutamisele. Meie jaoks on vaja aru saada 
kuivõrd Sa neid kasutada suudad või neid kasutama suudad õppida ning 
kuidas mõistad nende vahendite tähendust ning rolli tarkvaraarenduse 
juures. Usume, et seda teadmist, ükskõik kas töötad kunagi meie juures 
või kuskil mujal, on Sul kindlasti tulevikus vaja.

Kui võtad proovitöö vastu, siis võta ka rahulikult aega. Kiiret kuhugi 
ei ole. Me ei eelda, et selle väga ruttu valmis saad kuna kindlasti on 
nii mõnigi asi Sulle selles komplektis üsna uus ja tundmatu. Tähtis on, 
et aru saad miks ja kuidas neid vahendeid kasutatakse. Need on meie 
igapäevatöös nö. põhivahendid. Me ei eelda Sinu poolt nüansside tundmist 
aga üldist arusaamist ja käsitlemisoskust küll.